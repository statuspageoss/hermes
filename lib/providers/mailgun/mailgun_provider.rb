module Hermes
  class MailgunProvider < Provider
    required_credentials :api_key

    def send_message(rails_message)
      domain = extract_custom(rails_message, :mailgun_domain)
      domain = self.defaults[:domain] if domain.blank?
      # Utils.log_and_puts "Extracted domain:#{domain} from custom:#{extract_custom(rails_message, :mailgun_domain).class} default:#{self.defaults[:domain]}"
      message = self.mailgun_message(rails_message)

      if self.deliverer.should_deliver?
        self.client.send_message(domain, message)
      end

      rails_message
    end

    def mailgun_message(rails_message)
      message = Mailgun::MessageBuilder.new

      # basics
      message.set_from_address(extract_from(rails_message))
      extract_to_emails(rails_message).each do |to|
        message.add_recipient(:to, to.address, self.get_mailgun_name_options(to))
      end

      extract_cc(rails_message).each do |cc|
        message.add_recipient(:cc, cc.address, self.get_mailgun_name_options(cc))
      end

      message.set_subject(rails_message[:subject])
      message.set_html_body(extract_html(rails_message))
      message.set_text_body(extract_text(rails_message))
      message.set_message_id(rails_message.message_id)

      # optionals
      if rails_message[:reply_to]
        parsed_reply_to = Mail::Address.new(rails_message[:reply_to].value)
        message.add_recipient("h:reply-to", parsed_reply_to.address, self.get_mailgun_name_options(parsed_reply_to))
      end

      # and any attachments
      rails_message.attachments.try(:each) do |attachment|
        at = Hermes::EmailAttachment.new(attachment)
        message.add_attachment(Hermes::EmailAttachment.new(attachment), at.original_filename)
      end

      message
    end

    def get_mailgun_name_options(address)
      options = {}
      first, last = (address.name || '').split(' ', 2)
      options['first'] = first unless first.nil?
      options['last'] = last unless last.nil?
      options
    end

    def client
      Mailgun::Client.new(self.credentials[:api_key])
    end
  end
end