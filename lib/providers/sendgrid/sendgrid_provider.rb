module Hermes
  class SendgridException < StandardError
    def initialize(response)
      @response = response
    end

    def message
      @response.body
    end
  end

  class SendgridProvider < Provider
    required_credentials :api_key, :api_host
    
    def send_message(rails_message)
      payload = payload(rails_message)

      if self.deliverer.should_deliver?
        response = api.client.mail._('send').post(request_body: payload)
        raise SendgridException.new(response) unless (200..299).include?(response.status_code.to_i)
      end

      rails_message
    end

    def payload(rails_message)

      message = SendGrid::Mail.new

      message.subject = rails_message.subject
      text_body = extract_text(rails_message)
      html_body = extract_html(rails_message)
      message.contents = SendGrid::Content.new(type: 'text/plain', value: text_body) if text_body
      message.contents = SendGrid::Content.new(type: 'text/html', value: html_body) if html_body

      parsed_from_address = Mail::Address.new(extract_from(rails_message))
      message.from = SendGrid::Email.new(email: parsed_from_address.address, name: parsed_from_address.name)

      personalization = SendGrid::Personalization.new

      extract_to_emails(rails_message).each do |email|
        personalization.to = SendGrid::Email.new(email: email.address, name: email.name)
      end

      extract_cc(rails_message).each do |email|
        personalization.cc = SendGrid::Email.new(email: email.address, name: email.name)
      end

      if rails_message[:reply_to]
        parsed_reply_to_address = Mail::Address.new(rails_message[:reply_to].value)
        message.reply_to = SendGrid::Email.new(email: parsed_reply_to_address.address, name: parsed_reply_to_address.name)
      end

      if rails_message.attachments.present?
        rails_message.attachments.each do |attachment|
          hermes_attachment = Hermes::EmailAttachment.new(attachment)
          attachment = SendGrid::Attachment.new
          attachment.content = Base64.encode64(hermes_attachment.read)
          attachment.type = hermes_attachment.content_type
          attachment.filename = hermes_attachment.original_filename
          message.attachments = attachment
        end
      end

      message.personalizations = personalization

      # TODO: use message.headers = headers when sendgrid-ruby supports serializing the headers to the payload.
      message.to_json.merge("headers" => {"Message-ID" => rails_message[:message_id].value}) if rails_message[:message_id]
    end

    def api
      SendGrid::API.new({
        api_key: self.credentials[:api_key],
        host: self.credentials[:api_host]
      })
    end
  end
end