module Hermes
  class TwitterCredentials
    attr_reader :oauth_token, :oauth_token_secret
    def initialize(oauth_token, oauth_token_secret)
      @oauth_token = oauth_token
      @oauth_token_secret = oauth_token_secret
    end

    def client
      Twitter::Client.new(
        oauth_token: @oauth_token,
        oauth_token_secret: @oauth_token_secret
      )
    end
  end

  class TwitterProvider < Provider
    required_credentials :consumer_key, :consumer_secret
    
    def send_message(rails_message)
      body = extract_text(rails_message)

      if self.deliverer.should_deliver?
        self.client(rails_message).update(body)
      end
    end

    def client(rails_message)
      client = extract_to(rails_message)
      client = client.client if client.is_a?(TwitterCredentials)

      # just need to set the consumer key and secret and
      # then we'll be ready for liftoff
      client.consumer_key = self.credentials[:consumer_key]
      client.consumer_secret = self.credentials[:consumer_secret]

      return client
    end
  end
end