class SandboxMailer < ActionMailer::Base
  default from: 'Tyus Jones <tyus@duke.edu>'

  def nba_declaration(team_hopeful)
    @team_hopeful = team_hopeful

    attachments[Pathname.new(__FILE__).split[1].to_s] = {
      mime_type: 'text/plain',
      content: File.read(__FILE__)
    }

    mail to: 'Mike Krzyzewski <satan@duke.edu>',
         subject: 'leaving for the nba lol, good luck next year'
  end

  def nba_declaration_multi(team_hopeful)
    @team_hopeful = team_hopeful

    attachments[Pathname.new(__FILE__).split[1].to_s] = {
      mime_type: 'text/plain',
      content: File.read(__FILE__)
    }

    mail to: ['Mike Krzyzewski <satan@duke.edu>', 'Some Dude <shoes@hoops.bball>'],
         cc: ["First Name <first@name.com>", "Second Name <second@name.com>"],
         subject: 'leaving for the nba lol, good luck next year',
         reply_to: "No Reply <no-reply@blackh.ole>",
         message_id: "<8FBE98D8-517E-4E52-BBEB-B09B2A4BC601@example.com>" do |format|
      format.text { render "nba_declaration" }
      format.html { render "nba_declaration" }
    end
  end

  def nba_declaration_with_filter(team_hopeful, filter)
    @team_hopeful = team_hopeful
    
    # notice the singular here
    mail(hermes_provider: filter, to: 'Mike Krzyzewski <satan@duke.edu>', subject: 'leaving for the nba lol, good luck next year')
  end

  def nba_declaration_with_filters(team_hopeful, filters)
    @team_hopeful = team_hopeful
    
    # notice the plural here
    mail(hermes_providers: filters, to: 'Mike Krzyzewski <satan@duke.edu>', subject: 'leaving for the nba lol, good luck next year')
  end
end