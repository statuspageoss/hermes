require 'test_helper'

describe Hermes::MailgunProvider do
  before do
    @settings = {
      config: {
        test: true,
        mappings: {
          email: String
        },
        stats: Hermes::CompleteStatsHandler
      },
      email: {
        mailgun: {
          credentials: {
            api_key: 'mailgun_api_key'
          },
          defaults: {
            domain: 'mailgun_default_domain'
          },
          weight: 1
        },
      }
    }

    @deliverer = Hermes::Deliverer.new(@settings)
  end

  it "doesn't fail to create a mailgun message" do
    reset_email!
    msg = SandboxMailer.nba_declaration('Houston Rockets')
    @deliverer.deliver!(msg)
    assert_equal 1, email_count
    assert_equal('Mike Krzyzewski <satan@duke.edu>', ActionMailer::Base.deliveries.first.message[:to].value)
    assert_equal('Tyus Jones <tyus@duke.edu>', ActionMailer::Base.deliveries.first.message[:from].value)
  end

  it "doesn't fail to create a mailgun message with multiple target addresses" do
    reset_email!
    msg = SandboxMailer.nba_declaration_multi('Houston Rockets')
    @deliverer.deliver!(msg)
    assert_equal 1, email_count
    assert_equal(['Mike Krzyzewski <satan@duke.edu>', 'Some Dude <shoes@hoops.bball>'], ActionMailer::Base.deliveries.first.message[:to].value)
    assert_equal(["First Name <first@name.com>", "Second Name <second@name.com>"], ActionMailer::Base.deliveries.first.message[:cc].value)
    assert_equal('Tyus Jones <tyus@duke.edu>', ActionMailer::Base.deliveries.first.message[:from].value)
    assert_equal('No Reply <no-reply@blackh.ole>', ActionMailer::Base.deliveries.first.message[:reply_to].value)
  end

  it "builds the message payload correctly" do
    msg = SandboxMailer.nba_declaration_multi('Houston Rockets')
    payload = JSON.parse(Hermes::MailgunProvider.new(nil, {credentials:{api_key: :whatever}}).mailgun_message(msg).to_json)
    payload["message"]["from"].must_equal ["Tyus Jones <tyus@duke.edu>"]
    payload["message"]["subject"].first["value"].must_equal "leaving for the nba lol, good luck next year"
    payload["message"]["html"].must_equal ["Hi Mike, just wanted to let you know I'm hoping to be drafted by the <b>Houston Rockets</b>"]
    payload["message"]["text"].must_equal ["Hi Mike, just wanted to let you know I'm hoping to be drafted by the *Houston Rockets*"]
    payload["message"]["to"].must_equal ["'Mike Krzyzewski' <satan@duke.edu>", "'Some Dude' <shoes@hoops.bball>"]
    payload["message"]["cc"].must_equal ["'First Name' <first@name.com>", "'Second Name' <second@name.com>"]
    payload["message"]["h:reply-to"].must_equal ["'No Reply' <no-reply@blackh.ole>"]

    # Mailgun-ruby strips out the angle brackets on both side of the message id.
    payload["message"]["h:Message-Id"].must_equal "8FBE98D8-517E-4E52-BBEB-B09B2A4BC601@example.com"

    # Really frustrating they don't let you set content type or filename.
    # The client appears to support it but the API doesn't.
    payload["message"]["attachment"].count.must_equal 1
  end
end