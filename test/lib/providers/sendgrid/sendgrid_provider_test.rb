require 'test_helper'

describe Hermes::SendgridProvider do
  before do
    @settings = {
      config: {
        test: true,
        mappings: {
          email: String
        },
        stats: Hermes::CompleteStatsHandler
      },
      email: {
        sendgrid: {
          credentials: {
            api_key: 'sendgrid_api_key',
            api_host: "example.com"
          },
          weight: 1
        },
      }
    }

    @deliverer = Hermes::Deliverer.new(@settings)
  end

  it "doesn't fail to create a sendgrid message" do
    reset_email!
    msg = SandboxMailer.nba_declaration('Houston Rockets')
    @deliverer.deliver!(msg)
    assert_equal 1, email_count
    assert_equal('Mike Krzyzewski <satan@duke.edu>', ActionMailer::Base.deliveries.first.message[:to].value)
    assert_equal('Tyus Jones <tyus@duke.edu>', ActionMailer::Base.deliveries.first.message[:from].value)
  end

  it "doesn't fail to create a sendgrid message with multiple to and cc addresses" do
    reset_email!
    msg = SandboxMailer.nba_declaration_multi('Houston Rockets')
    @deliverer.deliver!(msg)
    assert_equal 1, email_count
    assert_equal(['Mike Krzyzewski <satan@duke.edu>', 'Some Dude <shoes@hoops.bball>'], ActionMailer::Base.deliveries.first.message[:to].value)
    assert_equal(["First Name <first@name.com>", "Second Name <second@name.com>"], ActionMailer::Base.deliveries.first.message[:cc].value)
    assert_equal('Tyus Jones <tyus@duke.edu>', ActionMailer::Base.deliveries.first.message[:from].value)
    assert_equal('No Reply <no-reply@blackh.ole>', ActionMailer::Base.deliveries.first.message[:reply_to].value)
  end

  it "builds the message payload correctly" do
    msg = SandboxMailer.nba_declaration_multi('Houston Rockets')
    payload = Hermes::SendgridProvider.new(nil, {credentials:{api_key: :whatever, api_host: :whatever}}).payload(msg)

    payload["subject"].must_equal "leaving for the nba lol, good luck next year"
    payload["content"].must_equal [
      {"type"=>"text/plain", "value"=>"Hi Mike, just wanted to let you know I'm hoping to be drafted by the *Houston Rockets*"},
      {"type"=>"text/html", "value"=>"Hi Mike, just wanted to let you know I'm hoping to be drafted by the <b>Houston Rockets</b>"}
    ]
    payload["from"].must_equal("email" => "tyus@duke.edu", "name" => "Tyus Jones")
    payload["personalizations"].first["to"].must_equal [
      {"email" => "satan@duke.edu", "name" => "Mike Krzyzewski"},
      {"email" => "shoes@hoops.bball", "name" => "Some Dude"}
    ]
    payload["personalizations"].first["cc"].must_equal [
       {"email" => "first@name.com", "name" => "First Name"},
       {"email" => "second@name.com", "name" => "Second Name"}
    ]
    payload["reply_to"].must_equal("email" => "no-reply@blackh.ole", "name" => "No Reply")

    # I have zero clue why :headers is a symbol instead of a string.
    payload["headers"]["Message-ID"].must_equal "<8FBE98D8-517E-4E52-BBEB-B09B2A4BC601@example.com>"
    payload["attachments"].size.must_equal 1
    assert payload["attachments"].first["content"].present?
    payload["attachments"].first["type"].must_equal "text/plain"
    payload["attachments"].first["filename"].must_equal "sandbox_mailer.rb"
  end
end